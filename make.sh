#!/bin/sh

basedir=$(dirname "$0")

file_name=$(basename $1)
file_named=${file_name%.*}
dir_name=$(dirname $1)




nasm -f elf64 $1 -l $basedir/bin/$file_named.lst -o $basedir/bin/$file_named.o
ld -s -o $basedir/bin/$file_named $basedir/bin/$file_named.o

echo $basedir/bin/$file_named.lst to list
echo $basedir/bin/$file_named.o to elf64 .o file
echo $basedir/bin/$file_named to execute
