bits 64			; 64bit モードの指定
section .text		; ELF形式の実行ファイルのプログラムであると宣言
global 	_start		; プログラムの実行を開始する場所の指定


_start:			; ラベルにより、プログラムの開始
	xor	eax,	eax
	mov	edx,	eax
	inc	eax		; sys_write (01)
	mov	edi,	eax	; stdout (01)
	mov	dl,	len	; length (13)
	mov	rsi,	msg	; address
	syscall
	xor	edi, 	edi	; return 0
	mov	eax, 	edi
	mov	al,	60	; sys_exit
	syscall

section .data		; 初期化に必要なデータ
	msg	db	"hello, world", 0x0a
	len	equ	$ - msg

section .bss		; 初期化に不要なデータ
	