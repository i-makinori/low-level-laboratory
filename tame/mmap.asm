
%define READONLY 0
%define PROT_READ 0x1
%define MAP_PRIVATE 0x2

section .data
fname: db "test.txt", 0

;; open system call
; regi	value	mean (regi means register)
; rax	2	syscamcall open. if scusess, file diskpriter returned
; rdi	"test.text",0	name of file. end to null character
; rsi	0	permission of read/write. 0 is only read
; rdx	0	permission to file making. 0 because rsi is 0,read only

;; mmap system call
; regi	value	mean (regi means register)
; rax	9	syscamcall mmap. if scuess, head address returned
; rdi	0	opened page address. 0 to assign auto
; rsi	4096	region size
; rdx	1	type of protect memory .0 is only read
; r10	2	used type (shared or private or anonymouse or ...)
; r8	fd	file diskpriter
; r7	0	offset of file




section .text
global _start

print_string:
	push rdi
	call string_length
	pop rsi
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall
	ret

;; count words in rdi
;; return value in rax
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi+rax], 0	;; null character
	je .end
	inc rax
	jmp .loop
.end:
	ret


_start:
;; call open
mov rax, 2 
mov rdi, fname
mov rsi, READONLY
mov rdx, 0
syscall

; file's discpriter are saved to rax by open. save.
mov r8, rax

; call mmap
mov rax, 9
mov rdi, 0
mov rsi, 1024
mov rdx, PROT_READ
mov r10, MAP_PRIVATE
mov r9, 0
syscall

; address regiond by mmap are saved to mmap. save and print
mov rdi, rax
call print_string

; exit
mov rax, 60
xor rdi, rdi
syscall

