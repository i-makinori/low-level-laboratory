section .data
str_fizz: db "fizz"
str_buzz: db "buzz"
str_numbers: db "0123456789"
str_newline: db 10

section .text
global _start

; print fizz
print_fizz:
	mov rax, 1
	mov rdi, 1
	mov rsi, str_fizz
	mov rdx, 4
	
	push rcx
	syscall
	pop rcx
	
	ret

; print buzz
print_buzz:
	mov rax, 1
	mov rdi, 1
	mov rsi, str_buzz
	mov rdx, 4
	
	push rcx
	syscall
	pop rcx
	
	ret

; print number as 10x 3digit string
print_number:
	; counter div 100
	mov rdx, 0
	mov rax, rcx
	mov rbx, 100
	div rbx
	; 
	push rdx
	call print_rax_number
	
	; moduler/10
	pop rax
	mov rdx, 0
	mov rbx, 10
	div rbx
	;
	push rdx
	call print_rax_number
	
	; show moduler as first digit
	pop rax
	call print_rax_number

	ret

print_rax_number:
	lea rsi, [str_numbers + rax]
	mov rax, 1
	mov rdi, 1
	mov rdx, 1
	
	push rcx
	syscall
	pop rcx

	ret

print_newline:
	mov rax, 1
	mov rdi, 1
	mov rsi, str_newline
	mov rdx, 1
	
	push rcx
	syscall
	pop rcx

	ret

check_fizz:
	mov rdx, 0
	mov rax, rcx
	mov rbx, 3
	div rbx
	cmp rdx, 0
	ret

check_buzz:
	mov rdx, 0
	mov rax, rcx
	mov rbx, 5
	div rbx
	cmp rdx, 0
	ret

exit:
	mov rax, 60
	xor rdi, rdi
	syscall


_start:
	mov rcx, 1	; couter to 1
	
	.loop:
		; check fizz and jump
		call check_fizz
		je .print_fizz
		
		; check buzz if not fizz
		call check_buzz
		je .print_buzz

		; print number in 10X if (!fizz) && (!buzz)
		.print_default:
			call print_number
			call print_newline
			jmp .next
		
		;; show fizz
		.print_fizz:
			call print_fizz

			; check if fizzbuzz
			call check_buzz
			je .print_buzz
			
			call print_newline
			jmp .next
		
		;; show buzz
		.print_buzz:
			call print_buzz
			call print_newline
			jmp .next

		.next:
			inc rcx
			cmp rcx, 100
			jle .loop

		call exit
		
	
		