
; asm:true.asm
global _start
	; global directive opens designated symbol 
	; _start ... word start with . or _ are called symbol, have 32bit value.
	; symbol last with : is called label, which have address of where defined.
	; _start is defined label (shown in bellow,) have address of there.

section .text
	; section directive defeines means of program while next section's region.
	; because section .text 's code are defined as disrewriteable data.
	;   program or constant are written to here
	; there is another sections

_start:
	mov rdi, 0	; mov A, B copy value from B to A
			; move 0 from 0 to rdi register
			; register is prepared memory region
			; rdi is destination index register, which have 64bit length
			; rdi roles first argument when syscall executing
	mov rax, 60	; move 60 from 60 to rax register
			; rax is called accumulator
			; rax's value are referd when syscall executing.
			; 60's syscall is exit
			; ausyscall --dump to print all of syscall
	syscall		; syscall calls operating system's function
			; need to be prepared before
		; when syscall here, rax's value 60...exit are executed
		; exit returns rdi's calue 0, and exit program with exit code 0.


