section .data

numbers: db "0123456789", 10
strings: db "abcdefghij", 10

section .text
global _start

; n: XXX :: n'th argument for syscall means XXX
;
; define function is only label, but use ret
print_numbers:
	mov rax, 1	; 0: write
	mov rdi, 1	; 1: stdout
	
	; slide output character counter value
	mov rsi, numbers
	add rsi, rcx	; 2: message
	mov rdx, 1	; 3: length
	push rcx
	syscall
	pop rcx
	ret		; return rcx


print_strings:
	mov rax, 1	; 0: write
	mov rdi, 1	; 1: stdout

	; lea A, B
	lea rsi, [strings + rcx]	; message
			; strings::byte*10, address of strings points where first byte of strings are
	mov rdx, 1	; length
	push rcx
	syscall
	pop rcx
	ret		; return rex


;; memo
;; mov is copy of value,
;; lea is copy of address.
;
; mov rax, label 	;; set label's address to rax
; ; equal 
; lea rax, [label] 	;; set seen label's address to rax
;
;; and also,
;
; mov rax, label	;; rax <- label
; add rax, rcx		;; rax <- rax+rcx
; ; equal
; lea rax, [label + rcx] ;; rax <- ([label]+[rcx])


exit:
	mov rax, 60	; 0: exit
	xor rdi, rdi	; 1: exit-mode mov rdi 0
	syscall


_start:
	mov rcx, 0

.loop:
	; call function
	call print_numbers
	call print_strings

	; jump by number compareing
	; jle means Jump if Less or Equal 
	inc rcx
	cmp rcx, 10
	jle .loop
	
	call exit
	