section .data

message: db "hello world", 10	; 10 is \newline in ASCII code
message_length: equ $ - message

section .text

global _start

_start:
	mov rax, 1		; syscall number 1 is write			; value: 60
	mov rdi, 1		; 1st argument. 1 is stdout			; value: 1	standart output
	mov rsi, message	; 2nd argument. input data start address	; value: hello world\n		address of defined message
	mov rdx, message_length	; 3rd argument. input data length		; value: 13	length of message
	syscall

	mov rax, 60
	xor rdi, rdi
	syscall
