; jump push pop

section .data


message: db "hello world" ,10
message_length: equ $ - message


section .text
global _start

_start:
	mov rcx, 3	; rcx register as counter. inital with 3

.loop:
	dec rcx		; decriment rcx
	
	; write system call
	mov rax, 1
	mov rdi, 1
	mov rsi, message
	mov rdx, message_length
	
	; push command to save counter to stack
	; evacuation because syscall's return are saved to rcx
	push rcx
	; execute write
	syscall

	; reinstatement
	pop rcx

	; test if counter is 0
	; if 0, zero flag
	test rcx, rcx
	; Jump if Not Zero
	jnz .loop

	; exit
	mov rax, 60
	xor rdi, rdi 		; faster than mov rdi 0
	syscall
	
	
	
