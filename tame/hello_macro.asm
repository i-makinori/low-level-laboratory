; %macro name args_length to % end are define of argd value argd tamplating
; name args to call after defined
; %1, %2 to access argvs

%macro print_string 2
mov rax, 1	; syscall 1 is write
mov rdi, 1	; stdout
mov rsi, %1	; string
mov rdx, %2	; length of string
push rcx	
syscall
pop rcx
%endmacro

section .data
hello: db "hello world", 10
hi: db "hi world", 10

section .text
global _start

_start:
	; %assign A B to B's calucuation result to A
	%assign i 0
	
	; loop NUM number from %rep NUM to %endrep 
	%rep 10
		; %if ... [%else ... ] %endif 
		%if i % 2 = 0
			print_string hi, 9
		%else
			print_string hello, 12
		%endif
		
		%assign i i+1
	%endrep

	mov rax, 60
	xor rdi, rdi
	syscall

